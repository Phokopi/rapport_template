# Template de rapport

Exemple de template pour un stage en école d'ingénieur, particulièrement adapté à la rédaction d'un mémoire.
Très facile à adapter pour un rapport de stage 1A ou 2A, voire pour un rapport de projet.

Possibilité d'upload le dossier complet sur [https://www.overleaf.com/](https://www.overleaf.com/), c'est assez pratique pour faire sur n'importe quel ordi (donc depuis le taf aussi !).

Sinon, pour compiler : `pdflatex main.tex` (à faire en général 3 fois d'affilé pour bien avoir la mise à jour du sommaire).
Je recommande [MiKTeX](https://miktex.org/) si vous faites ça en local. Ça s'intègre bien avec le package [atom-latex](https://atom.io/packages/atom-latex)  de l'éditeur de texte [Atom](https://atom.io/) (en utilisant une *custom toolchain* et en enlevant le %BIB).

*(Attention, le lecteur pdf intégré à gitlab n'affiche pas toujours les pages du fichier `main.pdf` dans le bon ordre !)*
